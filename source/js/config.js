require.config({
    baseUrl : "libs",
    paths: {
      jquery : 'jquery/dist/jquery',
      bootstrap : 'bootstrap-sass/assets/javascripts/bootstrap',
      slick : 'slick-carousel/slick/slick',
      mCustomScroll : 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar',
      mouseWheel : 'jquery-mousewheel/jquery.mousewheel',
    },
    shim: {
      jquery: {
        exports: 'jQuery'
      },
      bootstrap : {
        deps: [ 'jquery'],
      },
      slick : {
        deps: [ 'jquery'],
        exports: 'jQuery.fn.slick'
      },
      mCustomScroll : {
        deps: [ 'jquery', 'mouseWheel' ],
        exports: 'jQuery.fn.mCustomScrollbar'
      }
    }
});
